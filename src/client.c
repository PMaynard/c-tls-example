/*
Created By:  Peter Maynard <pete@port22.co.uk>
Created On:  2020 March 12th
Description: A sample TLS client written in C.
License:     BSD-3-Clause
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <tls.h>

#include <err.h>

#include "client.h"

int main(int argc, char **argv) {
  struct tls *ctx;
  struct tls_config *config;
  ssize_t buff_size = 200;
  ssize_t ret = 0;
  char buf[buff_size];
  const char http_msg[] = "GET / HTTP/1.1\r\nHost: example.com\r\n\r\n";

  /* https://man.openbsd.org/tls_client.3 */
  if ((ctx = tls_client()) == NULL)
    errx(1, "tls_client: %s", tls_error(ctx));

  /* https://man.openbsd.org/tls_config_new.3 */
  if ((config = tls_config_new()) == NULL)
    errx(1, "tls_config_new: %s", tls_error(ctx));

  /* https://man.openbsd.org/tls_configure.3 */
  if (tls_configure(ctx, config) == -1)
    errx(1, "tls_configure: %s", tls_error(ctx));

  /* https://man.openbsd.org/tls_connect.3 */
  if (tls_connect(ctx, "example.com", "443") == -1)
    errx(1, "tls_connect: %s", tls_error(ctx));

  /* https://man.openbsd.org/tls_write.3 */
  if ((ret = tls_write(ctx, http_msg, strlen(http_msg))) == -1)
    errx(1, "tls_write: %s", tls_error(ctx));
  printf("- Sent '%zd'\n", ret);

  /* https://man.openbsd.org/tls_read.3 */
  if ((ret = tls_read(ctx, buf, buff_size)) == -1)
    errx(1, "tls_read: %s", tls_error(ctx));

  printf("- Received '%zd\n\n", ret);
  printf("%s\n\n", buf);

  if (ret == buff_size) {
    printf("- We have reached the maximum buffer size.\n- Some of the response "
           "may have been truncated.\n");
  }

  /* https://man.openbsd.org/tls_close.3 */
  if (tls_close(ctx) == -1)
    errx(1, "tls_close: %s", tls_error(ctx));

  /* https://man.openbsd.org/tls_free.3 */
  tls_free(ctx);

  return (EXIT_SUCCESS);
}
